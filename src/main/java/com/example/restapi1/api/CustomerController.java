package com.example.restapi1.api;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

@SpringBootApplication
@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    ArrayList<Customer>  customers = new ArrayList<>();

    public  CustomerController() {
        customers.add(new Customer("phearun","male",21,"PP"));
        customers.add(new Customer("phearum","male",22,"PP"));
        customers.add(new Customer("Johnny","male",21,"Kandal"));
    }
    @GetMapping
    public ResponseEntity<?>  getAllPersons(){
        return ResponseEntity.ok(new CustomerRespon<ArrayList<Customer>>(
                "This record was successfully created",
                customers,
                "ok",
                LocalDateTime.now()
        ));
    }
    @PostMapping
    public ResponseEntity<?>  insertCustomer (@RequestBody Scustomer customer){
        customers.add(new Customer(customer));
        return ResponseEntity.ok(new CustomerRespon<ArrayList<Customer>>(
                "This record was successfully created",
                customers,
                "ok",
                LocalDateTime.now()

        ));
    }
    @GetMapping("/{customerId}")
    public ResponseEntity<?> getId(@PathVariable("customerId")Integer cusId){
        for (Customer cus : customers ) {
            if (cus.getId()== cusId){
                return ResponseEntity.ok(new CustomerRespon<Customer>(
                        "This record has found successfully",
                        cus,
                        "ok",
                        LocalDateTime.now()
                ));
            }
        }
        return new ResponseEntity<>("ID Not found ",HttpStatus.NOT_FOUND);
    }
    @GetMapping("/search")
    public ResponseEntity<?> getByname(@RequestParam String CusName){
        for (Customer cus: customers) {
            if(Objects.equals(cus.getName(),CusName)){
                return ResponseEntity.ok(new CustomerRespon<Customer>(
                        "This record has found successfully",
                        cus,
                        "ok",
                        LocalDateTime.now()
                ));
            }
        }
        return new ResponseEntity<>("Name Not found ",HttpStatus.NOT_FOUND);
    }


    @PutMapping
    public ResponseEntity<?> updateCustomer (@RequestParam Integer uId, @RequestBody Scustomer customerss){
        for (Customer cus:customers ) {
            if (cus.getId()== uId){
                cus.setName(new Customer(customerss).getName());
                cus.setGender(new Customer(customerss).getGender());
                cus.setAge(new Customer(customerss).getAge());
                cus.setAddress(new Customer(customerss).getAddress());
                return ResponseEntity.ok(new CustomerRespon<Customer>(
                        "You're update successfully",
                        cus,
                        "ok",
                        LocalDateTime.now()
                ));
            }

        }
        return new ResponseEntity<>("ID Not found ",HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable ("customerId") Integer dId ){
        for (Customer cus : customers) {
            if (cus.getId()==dId){
                customers.removeIf(customer ->customer.getId()==dId);
                return ResponseEntity.ok(new Respon2<Customer>(
                    "Congratulation your delete is successfully",
                        200,
                        LocalDateTime.now()
                )
                );
            }
        }

        return new ResponseEntity<>("ID Not found ",HttpStatus.NOT_FOUND);

    }


}
