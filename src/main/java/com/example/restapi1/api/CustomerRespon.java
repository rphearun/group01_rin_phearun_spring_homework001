package com.example.restapi1.api;

import java.time.LocalDateTime;

public class CustomerRespon<T> {
    private String message ;
    private T customer ;

    private String status ;
    private LocalDateTime dateTime ;

    public CustomerRespon(String message, T customer, String status, LocalDateTime dateTime) {
        this.message = message;
        this.customer = customer;
        this.status = status;
        this.dateTime = dateTime;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }



    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}






