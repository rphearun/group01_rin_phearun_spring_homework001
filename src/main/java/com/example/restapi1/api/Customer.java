package com.example.restapi1.api;


public class Customer {

    public Customer(String name, String gender, int age, String address) {
        this.Id= increment++;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.address = address;
    }
    public Customer(Scustomer abc ) {
        this.Id= increment++;
        this.name = abc.name;
        this.gender = abc.gender;
        this.age = abc.age;
        this.address = abc.address;
    }
    public static int increment =1  ;
    private Integer Id ;
    private String name;
    private  String gender ;
    private Integer age ;
    private  String address;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id ;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



}
